-module(videohost_json).
-include_lib("stdlib/include/qlc.hrl").
-include("ivideohost.hrl").
-behavior(cowboy_http_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).



getAll()->
        F=fun()->
              qlc:eval(qlc:q(
                         [X || X <-mnesia:table(movie_url)]))
      end,
    {atomic, Values}=mnesia:transaction(F),
    Data=[ 
             [{url0, list_to_binary(Line#movie_url.url)}, 
              {filename, list_to_binary(string:sub_string(Line#movie_url.file, (string:rstr(Line#movie_url.file, "/") + 1) )     )},
               {category, list_to_binary(Line#movie_url.category)}] || Line <- Values],
    {ok, mochijson2:encode(Data)}.





init(_Type, Req, _Opts) ->
    {ok, Req, undefined_state}.

handle(Req, State) ->
    {_, _}=cowboy_req:binding(type, Req),
    {Name, _}=cowboy_req:binding(name, Req, <<"all">>),
    {ok, Data}=getAll(),
    {ok, Req3} = cowboy_req:reply(200, [
                                        {<<"content-type">>, <<"application/json">>}
                                       ], Data, Req),
    {ok, Req3, State}.

terminate(_Reason, _Req, _State) ->
    ok.
