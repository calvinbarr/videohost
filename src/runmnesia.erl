-module(runmnesia).
-include("ivideohost.hrl").
-include_lib("stdlib/include/qlc.hrl").


-export([start/0, stop/0,  init_records/0, select/1]).
-export([selectAll/0]).
-export([selectjson/0, selectjson2/0]).

start()->
    mnesia:create_schema([node()]),
    mnesia:start(),
    mnesia:create_table(movie_url, 
                        [{disc_copies, [node()]},
                         {attributes, 
                          record_info(fields, movie_url)}]),
    mnesia:create_table(movie_tag, 
                        [{disc_copies, [node()]},
                         {attributes, 
                          record_info(fields, movie_tag)}]),
    mnesia:create_table(url_movie, 
                        [{disc_copies, [node()]},
                         {attributes, 
                          record_info(fields, url_movie)}]),
    ok.

init_records()->
    Files=filelib:wildcard("../../**/*.erl"),
    Fun=fun()->
                insert(Files, 0)
        end,
    mnesia:transaction(Fun),
    ok.



stop()->
    ok.

%% private functions
%-record(movie_url, {movie_file, movie_url}).
%-record(movie_tag, {movie_file, tag}).
%-record(url_movie, {movie_url, movie_file}).
insert([], A)->
    A;
insert([H|T], A) ->    
    K=shortener:shorten(A),
    io:format("inserting movie file ~p~n" , [H]),
    mnesia:write(#movie_url{file=H, url=K, category="all"}),
    mnesia:write(#url_movie{url=K, file= H }),
    insert(T, A+1).


select(Movie)->
    Fun = fun()->
                  mnesia:match_object({movie_url, Movie, '_'})
          end,
    {atomic, Results}= mnesia:transaction(Fun),
    Results.

selectAll()->
    Fun = fun()->
                  mnesia:select(movie_url, [{'_', [], ['$_']}])
          end,
    mnesia:activity(transaction, Fun).



selectjson()->
    F= fun()->
               qlc:eval( 
                 qlc:q(
                   [X || X <-mnesia:table(movie_url)]
                   ))
       end,
    mnesia:transaction(F).


    
selectjson2()->
    F=fun()->
              qlc:eval(qlc:q(
                         [X || X <-mnesia:table(movie_url)]))
      end,
    {atomic, Values}=mnesia:transaction(F),
    Data=[ 
             [{url0, Line#movie_url.url}, {filename, Line#movie_url.file}] || Line <- Values],
    iolist_to_binary(mochijson2:encode(Data)).


