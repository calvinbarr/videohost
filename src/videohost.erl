-module(videohost).

-export([start/0, stop/0]).


ensure_started(App)->
    case  application:start(App) of
        ok ->
            ok;
        {error, {already_started, App}} ->
            io:format ( "Already Started ~n"),
            ok
end.

start()->
    ensure_started(crypto),
    ensure_started(sasl),
    ensure_started(ranch),
    ensure_started(cowlib),
    ensure_started(cowboy),
    mnesia:start(),
    ensure_started(videohost).
    
stop()->    
    application:stop(videohost).
