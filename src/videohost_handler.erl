-module(videohost_handler).
-behavior(cowboy_http_handler).

-export([init/3]).
-export([handle/2]).
-export([terminate/3]).

init(_Type, Req, _Opts) ->
    {ok, Req, undefined_state}.

handle(Req, State) ->
    Files=filelib:wildcard("../../**/*.erl"),
    {ok, Req2} = cowboy_req:reply(200, [
        {<<"content-type">>, <<"text/html">>}
                                       ], string:join(Files, "<br/>"), Req),
        {ok, Req2, State}.

terminate(_Reason, _Req, _State) ->
    ok.
