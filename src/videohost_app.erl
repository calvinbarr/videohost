-module(videohost_app).
-behavior(application).

-export([start/2]).
-export([stop/1]).

start(_Type, _Args) ->
    Dispatch = cowboy_router:compile(
                 [{'_', [{"/", videohost_handler, []},
                         {"/video/:name", videohost_video,[]},
                         {"/json/:type/[:name]", videohost_json, []},
                         {"/static/[...]", cowboy_static,
                          [{directory, {priv_dir, videohost, [<<"static">>]}},
                           {mimetypes, {fun mimetypes:path_to_mimes/2, default}}
                          ]}
                        ]}
                 ]),
    cowboy:start_http(my_http_listener, 100,
                      [{port, 8080}],
                      [{env, [{dispatch, Dispatch}]}]
                     ),
    videohost_sup:start_link().


stop(_State) ->
    ok.
