-module(shortener).

-export([shorten/1, expand/1]).



shorten(N)->
    shorten(N, []).

shorten(N, A) when N == 0 ->
    A;
shorten(N, A) ->
    shorten( trunc(N/62), [e(N rem 62)| A]).

expand(S) ->
    decode(lists:reverse(S), 0, 0).

decode([], V, _) ->
    V;
decode([H|T], V, D) ->
    decode(T, V + ( round(math:pow(62, D)) * d(H)), D+1).

    



%% internal function

e(C) when C >=0, C =< 9 -> 
    C + $0;
e(C) when C >=10, C =< 35 -> 
    C - 10 + $a; 
e(C) when C >= 36, C =< 61 ->
    C - 36 + $A.


d($0) -> 0;
d($1) ->  1; d($2) ->  2;
d($3) ->  3; d($4) ->  4; d($5) ->  5;
d($6) ->  6; d($7) ->  7; d($8) ->  8;
d($9) ->  9; d($a) -> 10; d($b) -> 11;
d($c) -> 12; d($d) -> 13; d($e) -> 14;
d($f) -> 15; d($g) -> 16; d($h) -> 17;
d($i) -> 18; d($j) -> 19; d($k) -> 20;
d($l) -> 21; d($m) -> 22; d($n) -> 23;
d($o) -> 24; d($p) -> 25; d($q) -> 26;
d($r) -> 27; d($s) -> 28; d($t) -> 29;
d($u) -> 30; d($v) -> 31; d($w) -> 32;
d($x) -> 33; d($y) -> 34; d($z) -> 35;
d($A) -> 36; d($B) -> 37; d($C) -> 38;
d($D) -> 39; d($E) -> 40; d($F) -> 41;
d($G) -> 42; d($H) -> 43; d($I) -> 44;
d($J) -> 45; d($K) -> 46; d($L) -> 47;
d($M) -> 48; d($N) -> 49; d($O) -> 50;
d($P) -> 51; d($Q) -> 52; d($R) -> 53;
d($S) -> 54; d($T) -> 55; d($U) -> 56;
d($V) -> 57; d($W) -> 58; d($X) -> 59;
d($Y) -> 60; d($Z) -> 61.
